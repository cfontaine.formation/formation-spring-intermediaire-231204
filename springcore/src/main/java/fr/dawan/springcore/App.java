package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.components.ArticleRepository;
import fr.dawan.springcore.components.ArticleService;

public class App 
{
    public static void main( String[] args )
    {
        // Exemple Lombok
//        Article a=new Article(3.5, "Stylo");
//        System.out.println(a.getPrix());
//        System.out.println(a);
        
        // Builder
//        Article.builder().prix(34.0).description("souris").build();
        // ----------------------------------------------------------
        // Spring Core
        // Création du conteneur d'ioc
        ApplicationContext ctx=new AnnotationConfigApplicationContext(AppConf.class);
        
        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        DtoMapper m1=ctx.getBean("mapper1",DtoMapper.class);
        System.out.println(m1);
        
        ArticleRepository r1=ctx.getBean("repository1",ArticleRepository.class);
        System.out.println(r1);
        
        ArticleRepository r2=ctx.getBean("repository2",ArticleRepository.class);
        System.out.println(r2);
        
        ArticleService srv1=ctx.getBean("service1",ArticleService.class);
        System.out.println(srv1);
        
        // Fermeture du context entraine la destruction de tous les beans
        ((AbstractApplicationContext) ctx).close();

    }
}
