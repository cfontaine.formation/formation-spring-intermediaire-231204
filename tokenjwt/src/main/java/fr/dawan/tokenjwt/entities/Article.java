package fr.dawan.tokenjwt.entities;

import fr.dawan.tokenjwt.enums.Emballage;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "article")
public class Article extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, length = 150)
    private String description;

    @Column(nullable = false)
    private double prix;

    @Enumerated(EnumType.STRING)
    @Column(length = 15, nullable = false)
    private Emballage emballage;

    @ManyToOne
    private Marque marque;

    public Article(String description, double prix, Emballage emballage) {
        this.description = description;
        this.prix = prix;
        this.emballage = emballage;
    }

}
