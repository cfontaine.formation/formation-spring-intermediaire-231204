package fr.dawan.tokenjwt.dtos;

import java.time.LocalDate;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MarqueDto {
    
    @NotNull
    @Min(1)
    private long id;
    
    @NotEmpty
    @Size(min = 2, max = 40)
    private String nom;
    
    @Past
    private LocalDate dateCreation;

    private byte[] logo;

}
