package fr.dawan.tokenjwt.dtos;

import fr.dawan.tokenjwt.enums.Emballage;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ArticleDtoPost {

    private long id;

    @NotEmpty
    @Size(min = 3, max = 150)
    private String description;

    @NotNull
    @PositiveOrZero
    private double prix;

    @NotNull
    private Emballage emballage;

    @NotNull
    @Min(1)
    private long marqueId;

}
