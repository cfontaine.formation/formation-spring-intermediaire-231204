package fr.dawan.tokenjwt.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class JwtRequestDto {
    
    private String username;
    
    private String password;

}
