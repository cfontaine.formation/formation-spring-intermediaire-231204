package fr.dawan.tokenjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.tokenjwt.dtos.JwtRequestDto;
import fr.dawan.tokenjwt.dtos.JwtResponseDto;
import fr.dawan.tokenjwt.security.JwtTokenUtil;

@RestController
public class JwtAuthorisationController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    private UserDetailsService userDetailService;
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    @PostMapping(value="/signin",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?>createToken(@RequestBody JwtRequestDto dtoRequest) throws Exception{
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(dtoRequest.getUsername(), dtoRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS",e);
        }
    
        UserDetails ud=userDetailService.loadUserByUsername(dtoRequest.getUsername());
        String jwtToken=jwtTokenUtil.generateJwtToken(ud);
        return ResponseEntity.ok(new JwtResponseDto(jwtToken));
    }
    
}
