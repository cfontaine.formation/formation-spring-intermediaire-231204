package fr.dawan.tokenjwt.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.tokenjwt.dtos.ApiError;
import fr.dawan.tokenjwt.dtos.ArticleDto;
import fr.dawan.tokenjwt.dtos.ArticleDtoPost;
import fr.dawan.tokenjwt.services.ArticleService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/articles")
public class ArticleController {

    @Autowired
    ArticleService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> getAllArticle(Pageable page) {
        return service.getAll(page);
    }

    @GetMapping(value = "/{id:[0-9]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArticleDto> getArticleById(@PathVariable long id) {
        Optional<ArticleDto> dto = service.getById(id);
        if (dto.isPresent()) {
            return ResponseEntity.ok(dto.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/{nom:^\\D\\w+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> getArticleByIdDescription(@PathVariable String nom) {
        return service.getByDescription(nom);
    }

    @GetMapping(value = "/marque/{nom}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> getArticleByMarqueNom(@PathVariable String nom, Pageable page) {
        return service.getByMarqueNom(nom, page);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiError> deleteArticleById(@PathVariable long id) {
        if (service.deleteById(id)) {
            return ResponseEntity.ok(new ApiError(HttpStatus.OK, "L'article a été supprimé"));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ApiError(HttpStatus.NOT_FOUND, "L'article n'existe pas"));
        }
    }
    
    @PostMapping(consumes= MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto createArticle(@RequestBody @Valid ArticleDtoPost articleDto) {
        return service.save(articleDto);
    }
    
    @PutMapping(value="/{id}",consumes= MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArticleDto> updateArticle(@PathVariable long id,@RequestBody @Valid ArticleDtoPost articleDto) {
        Optional<ArticleDto> dto = service.update(id,articleDto);
        if (dto.isPresent()) {
            return ResponseEntity.ok(dto.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
