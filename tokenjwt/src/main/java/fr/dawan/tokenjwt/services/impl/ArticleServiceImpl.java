package fr.dawan.tokenjwt.services.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.tokenjwt.dtos.ArticleDto;
import fr.dawan.tokenjwt.dtos.ArticleDtoPost;
import fr.dawan.tokenjwt.entities.Article;
import fr.dawan.tokenjwt.repositories.ArticleRepository;
import fr.dawan.tokenjwt.services.ArticleService;

@Service
@Transactional(readOnly = true)
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<ArticleDto> getAll(Pageable page) {
        return repository.findAll(page).stream().map(a -> mapper.map(a, ArticleDto.class)).toList();
    }

    @Override
    public Optional<ArticleDto> getById(long id) {
        return repository.findById(id)
                .map(a -> mapper.map(a, ArticleDto.class));      
    }

    @Override
    public List<ArticleDto> getByDescription(String nom) {
        return repository.findByDescriptionLike("%" + nom + "%").stream().map(a -> mapper.map(a, ArticleDto.class)).toList();
    }

    @Override
    public List<ArticleDto> getByMarqueNom(String nomMarque, Pageable page) {
        return repository.findByMarqueNom(nomMarque, page).stream().map(a -> mapper.map(a, ArticleDto.class)).toList();
    }

    @Transactional
    @Override
    public boolean deleteById(long id) {
        return repository.removeById(id)!=0;
    }

    @Transactional
    @Override
    public ArticleDto save(ArticleDtoPost articleDto) {
        return mapper.map(repository.saveAndFlush(mapper.map(articleDto, Article.class)), ArticleDto.class);
    }

    @Transactional
    @Override
    public Optional<ArticleDto> update(long id,ArticleDtoPost articleDto) {
       return Optional.of(repository.findById(id).map( a -> {
            a.setDescription(articleDto.getDescription());
            a.setPrix(articleDto.getPrix());
            return mapper.map(repository.saveAndFlush(a),ArticleDto.class);
        }).orElseGet(() -> mapper.map(repository.saveAndFlush(mapper.map(articleDto, Article.class)),ArticleDto.class)));

    }

}
