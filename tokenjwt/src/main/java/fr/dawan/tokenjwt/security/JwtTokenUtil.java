package fr.dawan.tokenjwt.security;

import java.io.Serializable;
import java.util.Date;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;


@Component
public class JwtTokenUtil implements Serializable {

 private static final long serialVersionUID = 1L;

 @Value("${jwt.secret}")
 private String secret;

 @Value("${jwt.expiration}")
 private int expiration;

 // Générer le token JWT
 public String generateJwtToken(UserDetails userDetail) {
     return Jwts.builder()
			.subject(userDetail.getUsername())
			.issuedAt(new Date())
            .expiration(new Date((new Date()).getTime() + expiration))
			.signWith(getSecretKey(),Jwts.SIG.HS256)
			.compact();
 }

 // Test la validité du token ( username extrait de la charge utile du token,
 // correspond au username du userDetails et le token n'est pas expiré)
 public boolean valideJwtToken(String token, UserDetails userDetail) {
     return getUsernameForToken(token).equals(userDetail.getUsername()) && !isTokenExpired(token);
 }

 // Extrait le userName de la charge utile du token (claim sub)
 public String getUsernameForToken(String token) {
     return extractAllClaims(token).getSubject();
 }

 // Permet de tester, si le token est expiré (claim exp)
 public boolean isTokenExpired(String token) {
     return extractAllClaims(token).getExpiration().before(new Date());
 }

 // Extrait tous les claims (claim -> paire clé/value)
 // de la charge utile (payload) du token
 private Claims extractAllClaims(String token) {
     return Jwts
			.parser()
			.verifyWith(getSecretKey()).build()
			.parseSignedClaims(token)
			.getPayload();
 }

 // Crée une instance de SecretKey à partir de la secret
 public SecretKey getSecretKey() {
       return Keys.hmacShaKeyFor(Decoders.BASE64.decode(secret));
 }
}
