package fr.dawan.tokenjwt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.tokenjwt.entities.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {

    int removeById(long id);
    
}
