package fr.dawan.tokenjwt.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.tokenjwt.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    
    List<Article> findByDescriptionLike(String modele);
    
    List<Article> findByMarqueNom(String nomMarque, Pageable page);
    
    int removeById(long id);

}
