package fr.dawan.tokenjwt;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TokenjwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokenjwtApplication.class, args);
	}

	@Bean
	ModelMapper modelMapper() {
	    return new ModelMapper();
	}
}
