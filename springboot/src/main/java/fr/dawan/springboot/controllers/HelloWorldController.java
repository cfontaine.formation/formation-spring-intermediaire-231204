package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    // @Value permet d'injecter dans un variable d'instance, une valeur
    // définie dans une propriétée d'application.properties
    @Value("${message.hello}")
    private String message;

    @RequestMapping("/hello")
    public String helloWorld() {
        return message;
    }

}
