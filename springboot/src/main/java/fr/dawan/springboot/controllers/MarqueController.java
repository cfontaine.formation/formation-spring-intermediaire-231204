package fr.dawan.springboot.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.MarqueService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/v1/marques")
@Tag(name="Marques",description = "L'api des marques")
public class MarqueController {
    
    @Autowired
    private MarqueService service;
    
    // localhost:8080/api/v1/marques
   // @RequestMapping(value="",method = RequestMethod.GET)
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE) // value="",
    List<MarqueDto> getAllMarque(){
       return service.getALLMarque(Pageable.unpaged());
    }
    
    // localhost:8080/api/v1/marques?page=1&size=2&tri=dateCreation&tri=nom,desc
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE,params= {"page","size"})
    List<MarqueDto> getAllMarque(Pageable page){
        return service.getALLMarque(page);
    }
    
    // localhost:8080/api/v1/marques/3
    
    @Operation(summary = "Trouver le marques en fonction de l'id",description="retourne des marques",tags="Marques")
    @ApiResponses({
        @ApiResponse(responseCode = "200", description="Opération réussit", content=@Content(schema = @Schema(implementation= MarqueDto.class))),
        @ApiResponse(responseCode = "404", description="Marque non trouvé")
        
    })
    @GetMapping(value="/{id:[0-9]+}",produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> getMarqueById(
            @Parameter(description="L'id de la marque",required = true,allowEmptyValue = false)
            @PathVariable long id) {
        try {
            return ResponseEntity.ok().body(service.getMarqueById(id));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    // localhost:8080/api/v1/marques/Marque
    @GetMapping(value="/{nom:[A-Za-z]+}",produces=MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> getMarqueByNom(@PathVariable String nom){
        return service.getMarqueByNom(nom);
    }
    
    // localhost:8080/api/v1/marques/3
    @DeleteMapping(value="/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteMarqueById(@PathVariable long id) {
        if(service.deleteMarque(id)) {
            return new ResponseEntity<String>("La marque id="+id+ " est suprimée", HttpStatus.OK);
    
        }
        else {
            return new ResponseEntity<String>("La marque id="+id+ " n'existe pas", HttpStatus.NOT_FOUND);
        }
            
    }
    
    @ResponseStatus(code=HttpStatus.CREATED)
    // localhost:8080/api/v1/marques
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto createMarque(@RequestBody MarqueDto marqueDto) {
        return service.save(marqueDto);
    }
    
    @PutMapping(value="/{id}",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto updateMarque(@PathVariable long id,@RequestBody MarqueDto marqueDto ) {
        marqueDto.setId(id);
        return service.update(marqueDto);
    }
    
    @GetMapping("/ioexception")
    public void genIoException() throws IOException {
        throw new IOException("Erreur E/S");
    }
    
    @GetMapping("/sqlexception")
    public void genSqlException() throws SQLException {
        throw new SQLException("Erreur SQL");
    }
    
    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handlerIoException(IOException e){
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
