package fr.dawan.springboot;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.dtos.ArticleDto;
import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.ArticleService;
import fr.dawan.springboot.services.MarqueService;

//@Component
//@Order(2)
public class ServiceRunner implements CommandLineRunner {

    @Autowired
    private MarqueService service;
    
    @Autowired
    private ArticleService articleService;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Service Runner");
        service.getALLMarque(Pageable.unpaged()).forEach(System.out::println);
        System.out.println("------------------------------------------------");
        MarqueDto m = new MarqueDto(0, "Marque L", LocalDate.of(2009, 10, 10));
        MarqueDto mr = service.save(m);
        System.out.println(mr);
        System.out.println("------------------------------------------------");
        mr.setNom("Marque Lu");
        mr = service.update(mr);
        System.out.println(mr);
        System.out.println("------------------------------------------------");
        long id = mr.getId();
        System.out.println(service.getMarqueById(id));
        System.out.println("------------------------------------------------");
        System.out.println(service.deleteMarque(id));
        System.out.println(service.deleteMarque(id));
        System.out.println("------------------------------------------------");
        service.getALLMarque(Pageable.unpaged()).forEach(System.out::println);
    
        articleService.getALL(PageRequest.of(1, 3)).forEach(System.out::println);
        ArticleDto aDto=articleService.getById(2L);
        System.out.println(aDto);
        aDto.setDescription(aDto.getDescription()+ " update");
        aDto=articleService.update(aDto, 2L);
        System.out.println(aDto);
    }

}
