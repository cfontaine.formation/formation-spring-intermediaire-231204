package fr.dawan.springboot;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.entities.relations.Article;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.enums.Emballage;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.repositories.MarqueCustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;

//@Component
//@Order(1)
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private MarqueRepository marqueRepository;
    
    @Autowired
    private MarqueCustomRepository customRepo;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Repository Runner");

        System.out.println("-----------------");
        List<Article> lst = articleRepository.findAll();
        for (Article a : lst) {
            System.out.println(a);
        }

        System.out.println("----------------------------");

        lst = articleRepository.findByPrix(20.0);
        lst.forEach(a -> System.out.println(a));

        System.out.println("----------------------------");

        lst = articleRepository.findByPrixLessThan(100.0);
        lst.forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        articleRepository.findByPrixGreaterThanAndEmballage(100.0, Emballage.CARTON)
                .forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        articleRepository.findByPrixBetween(50.0, 150.0).forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        articleRepository.findByEmballageIn(Emballage.SANS, Emballage.PLASTIQUE).forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        articleRepository.findByDescriptionLike("m__%").forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        articleRepository.findByPrixLessThanOrderByDescriptionDescPrix(1000.0).forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        Marque mb = marqueRepository.findById(2L).get();
        articleRepository.findByMarque(mb).forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        articleRepository.findByMarqueDateCreationAfter(LocalDate.of(2000, 1, 1)).forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        articleRepository.findByMarqueCharteCouleurLike("_8%").forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        System.out.println(articleRepository.findTopByOrderByPrixDesc());

        System.out.println("----------------------------");
        articleRepository.findTop3ByOrderByPrix().forEach(a -> System.out.println(a));

        System.out.println("----------------------------");

        // Persitence d'un objet article
        Marque md=new Marque("Marque D",LocalDate.of(2000, 4, 1));
        
        Article a1 = new Article("Stylo bille bleu", 1.5, Emballage.SANS);
        a1.setMarque(md);
        md.getArticles().add(a1);
        marqueRepository.saveAndFlush(md);
        System.out.println(a1);
        a1 = articleRepository.saveAndFlush(a1);
        System.out.println(a1);
        
        
        System.out.println("----------------------------");
        System.out.println(articleRepository.existsByEmballage(Emballage.PLASTIQUE));
        
        System.out.println("----------------------------");
        System.out.println(articleRepository.countByPrixGreaterThan(30.0));
        
        System.out.println("----------------------------");
       // articleRepository.deleteByMarque(md);
        
        // Test cascade -> PERSIST
        Marque mf=new Marque("Marque F",LocalDate.of(1974, 12, 11));
        Article a2 = new Article("Stylo bille rouge", 1.5, Emballage.SANS);
        a2.setMarque(mf);
        mf.getArticles().add(a2);
        System.out.println(a2);
        marqueRepository.saveAndFlush(mf);
        System.out.println(a2);
        
        System.out.println("----------------------------");
        // Test cascade -> REMOVE
        marqueRepository.deleteById(mf.getId());
        articleRepository.findAll().forEach(System.out::println);
    
        System.out.println("----------------------------");
        articleRepository.findPrixInfJPQL(30.0).forEach(System.out::println);
    
        System.out.println("----------------------------");
        articleRepository.findPrixInfJPQL2(30.0).forEach(System.out::println);
        
        System.out.println("----------------------------");
        articleRepository.findPrixInfJPQL3(300.0,Emballage.CARTON).forEach(System.out::println);
       
        System.out.println("----------------------------");
        articleRepository.findMarqueNom("Marque A").forEach(System.out::println);
       
        System.out.println("----------------------------");
        articleRepository.findFournisseurNom("Fournisseur 1").forEach(System.out::println);
        
        System.out.println("----------------------------");
        articleRepository.findPrixSupSQL(40.0).forEach(System.out::println);
    
        // Pagination
        Page<Article> p=articleRepository.findAllBy(PageRequest.of(1,3));

        System.out.println(p.getNumberOfElements());
        System.out.println(p.getSize());
        System.out.println(p.getNumber());
        System.out.println(p.getTotalElements());
        System.out.println(p.getTotalPages());
        
        p.getContent().forEach(System.out::println);
        
        articleRepository.findByPrixGreaterThan(20.0, PageRequest.of(1,5)).forEach(System.out::println);
        
        Sort sort=Sort.by(Sort.Order.by("description")).and(Sort.by(Direction.DESC, "prix"));
        articleRepository.findAll(PageRequest.of(0,4,sort)).getContent().forEach(System.out::println);
    
        articleRepository.findByPrixGreaterThan(20.0, Pageable.unpaged()).forEach(System.out::println);
   
        // Procedure Stockée
        int res=articleRepository.countInfMontant(20.0);
        System.out.println(res);
        
        res=articleRepository.get_count_by_prix(20.0);
        System.out.println(res);
        
        // Repository personalisé
        customRepo.findBy("Marque A", null).forEach(System.out::println);
        
        customRepo.findBy(null, LocalDate.of(2000, 4, 1)).forEach(System.out::println);

        customRepo.findBy("Marque C", LocalDate.of(2005, 3, 7)).forEach(System.out::println);
    }

}
