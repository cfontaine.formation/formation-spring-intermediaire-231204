package fr.dawan.springboot.security;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    
    @Bean
    PasswordEncoder encoder()
    {
            // return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
//        // Sans les autoritity
//        http.csrf(c -> c.disable())
//        .authorizeHttpRequests(auth -> auth
//                .requestMatchers(new AntPathRequestMatcher("/**","POST")).authenticated()
//                .requestMatchers(new AntPathRequestMatcher("/**","PUT")).authenticated()
//                .requestMatchers(new AntPathRequestMatcher("/**","DELETE")).authenticated()
//                .anyRequest().permitAll())
//                .httpBasic(Customizer.withDefaults());
//        return http.build();
        
        // Sans les autoritity
        http.csrf(c -> c.disable())
        .authorizeHttpRequests(auth -> auth
                .requestMatchers(new AntPathRequestMatcher("/**","GET")).hasAnyAuthority("READ","SUPER")
                .requestMatchers(new AntPathRequestMatcher("/**","POST")).hasAnyAuthority("CREATE","SUPER")
                .requestMatchers(new AntPathRequestMatcher("/**","PUT")).hasAnyAuthority("CREATE","SUPER")
                .requestMatchers(new AntPathRequestMatcher("/**","DELETE")).hasAnyAuthority("DELETE","SUPER")
                .anyRequest().permitAll())
                .httpBasic(Customizer.withDefaults());
        return http.build();
    }
}
