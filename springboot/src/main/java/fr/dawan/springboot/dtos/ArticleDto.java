package fr.dawan.springboot.dtos;

import fr.dawan.springboot.enums.Emballage;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

public class ArticleDto {
    @Include
    private long id;
    
    private double prix;
    
    private String description;
    
    private Emballage emballage;
}
