package fr.dawan.springboot.entities.relations;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity
@Table(name = "charte_graphiques")
public class CharteGraphique implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Include
    private long id;

    @Version
    private int version;

    @Column(length = 6, nullable = false)
    private String couleur;

    @Lob
    @Column(length = 65000)
    @Exclude
    private byte[] logo;

    // Relation @OneToOne bidirectionnelle
    // C'est la relation retour CharteGraphique -> Marque
    // pour la réaliser, on place sur la variable d'instance de type Marque
    // on utilise l'annotation @OneToOne avec l'attribut mappedBy qui a pour valeur
    // le nom de variable d'instance de l'autre coté de la relation -> ici charte
    // dans ce cas, on peut connaitre le marque à partir de la charte et la marque à partir de charte
    @OneToOne(mappedBy = "charte")
    @Exclude
    private Marque marque;
}
