package fr.dawan.springboot.entities.heritage;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
//pour SINGLE_TABLE
//Pour la classe CompteEpargne la valeur dans la colonne discriminator => CE
@DiscriminatorValue("CE")
@Table(name = "compte_epargne")
public class CompteEpargne extends Compte {

    private static final long serialVersionUID = 1L;

    @Column(name = "taux_epargne")
    private Double tauxEpargne;
}
