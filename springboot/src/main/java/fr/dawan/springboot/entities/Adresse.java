package fr.dawan.springboot.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

//Une classe intégrable va stocker ses données dans la table de l’entité mère ce qui va créer des colonnes supplémentaires

//On annote une classe intégrable avec @Embeddable

@Embeddable
public class Adresse {

    private String rue;

    @Column(length = 50)
    private String ville;

    @Column(length = 10)
    private String codePostal;
}
