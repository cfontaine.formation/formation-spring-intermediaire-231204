package fr.dawan.springboot.entities.heritage;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorType;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="compte")
// Héritage => trois façons d’organiser l’héritage

//1 - SINGLE_TABLE => Le parent et les enfants vont être placé dans la même table
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//Une colonne "Discriminator" définit le type de la classe enregistrée
@DiscriminatorColumn(name = "compte_discriminator",discriminatorType = DiscriminatorType.STRING)
//Pour la classe CompteBancaire la valeur dans la colonne discriminator => CB
@DiscriminatorValue("CB")

//2 - TABLE_PER_CLASS => le parent et chaque enfant auront leur propre table
//la table enfant aura les colonnes des variables d'instance de la classe parent
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

//3 - JOINED => on aura une jointure entre la table de la classe parent et la table de la classe enfant
//@Inheritance(strategy = InheritanceType.JOINED)

public class Compte implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
 // @GeneratedValue(strategy = GenerationType.AUTO) // 2 - GenerationType.IDENTITY ne fonction avec TABLE_PER_CLASS
    private long id;
    
    @Version
    private int version;
    
    @Column(length = 40,nullable=false)
    private String titulaire;
    
    private double Solde;
}
