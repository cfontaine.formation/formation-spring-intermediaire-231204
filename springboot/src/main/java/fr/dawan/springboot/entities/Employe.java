package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.dawan.springboot.enums.Contrat;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//Une entité doit :
//- être annoté avec @Entity
//- avoir un attribut qui représente la clé primaire annoté avec @Id
//- implémenter l'interface Serializable
//- avoir obligatoirement un contructeur par défaut

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "employes")   // L'annotation @Table permet de modifier le nom de la table, sinon elle prend le nom de la classe
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id // -> Clé primaire simple
//    @GeneratedValue(strategy = GenerationType.AUTO)       // 1 - l'ORM choisi la stratégie
    @GeneratedValue(strategy = GenerationType.IDENTITY)     // 2 - BDD -> auto_increment, identity, serial ...
//    @TableGenerator(name = "table_gen")                   // 3 - ORM
//    @GeneratedValue(strategy = GenerationType.TABLE, generator = "table_gen")
//    @SequenceGenerator(name="employe_seq")                // 4 -> BDD séquence
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="employe_seq")
    private long id;

    @Version     // -> Gestion de la concurrence Optimistic
    private int version;

    // L'annotation @Column permet pour définir plus précisément la colonne
    @Column(length = 50)
    private String prenom;  // l'attribut length permet modifier de la longueur d'une chaine de caractère ou d'un @lob

    // l'attribut nullable permet de définir si le champ peut être NULL par défaut true
    @Column(length = 50, nullable = false)
    private String nom;

    private double salaire;

    @Column(unique = true, nullable = false)
    private String email;

    // Par défaut tous les variables d'instances sont persistées,
    // celles annotées avec @Transient ou sont précédées du mot clef transient ne sont pas persistées 
    @Transient
    private int nePasPersister;

    // Une énumération peut être stocké sous forme numérique EnumType.ORDINAL (par défaut)
    // ou sous forme de chaine de caractères EnumType.STRING
    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private Contrat contrat;

    // name -> permet de nommer la colonne correspondant à la variable d'instance
    // sinon la colonne à le nom de la variable
    @Column(name = "date_naissance")
    private LocalDate dateNaissance;

    @Lob // => pour stocker des données binaires dans la bdd (image, ...) BLOB ou un long texte CLOB
    @Column(length = 65000) // l'attribut length permet de définir la taille du blob => ici BLOB
    private byte[] photo;   // BLOB -> tableau de byte , CLOB -> String ou tableau de caractère

    // @ Embedded => pour utiliser une classe intégrable
    @Embedded
    private Adresse adressePerso;

    // Pour utiliser plusieurs fois la même classe imbriqué dans la même entitée
    // On aura plusieurs fois le même nom de colonne dans la table => erreur
    // On pourra renommé les colonnes avec des annotations @AttributeOverride placées
    // dans une annotation @AttributeOverrides
    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "ville", column = @Column(name = "ville_pro", length = 50)),
            @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro", length = 10)) })
    private Adresse adressePro;
    
    // Mapping d'une collection simple (Integer, String ...)
    @ElementCollection // @ElementCollection -> par défaut, génére une table NomClasse_nomVariable
    @CollectionTable(name="commentaires",joinColumns = @JoinColumn(name="id_employe"))
    // @CollectionTable -> permet de personaliser de la table name: nom de la table, joinColumns -> nom de la colonne de jointure 
    @Column(name="commentaire") // personaliser le nom de la colonne contenant les données contenues dans la collection
    private List<String> commentaires=new ArrayList<>();
    
    @ElementCollection
    @CollectionTable(name="telephones",joinColumns = @JoinColumn(name="id_telephone"))
    @MapKeyColumn(name="type_telephone")
    // @MapKeyColumn -> pour renommer la colonne qui correspond à la clé de la map
    @Column(name="num_telephone") // renommer la colonne qui correspond à la valeur de la map
    private Map<String,String> telephones=new HashMap<>();
}
