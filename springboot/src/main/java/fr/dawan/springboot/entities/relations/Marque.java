package fr.dawan.springboot.entities.relations;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import fr.dawan.springboot.entities.audit.BaseAuditing;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true,callSuper=true)

@Entity
@Table(name = "marques")
public class Marque extends BaseAuditing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Include
    private long id;

    @Version
    private int version;

    @Column(length = 60, nullable = false)
    private String nom;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @OneToOne // => Relation 1,1
    // ici, Une marque n'a qu'une seul charte graphique et charte graphique ne concerne qu'une seule marque

    // Relation @OneToOne Unidirectionnel
    // on a uniquement une relation Marque -> CharteGraphique
    // et pas de relation CharteGraphique -> Marque, on n'a pas accés à la marque depuis CharteGraphique

    @Exclude // si on utilise @ToString, il faut ajouter l'annotation @Exclude pour les
             // attribut qui sont des entitées (au moins d'un coté de la relation)
             // pour qu'ils ne soient pas ajouté dans la méthode toString -> cycles

    private CharteGraphique charte;

    @OneToMany(mappedBy = "marque", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
    @Exclude
    private Set<Article> articles = new HashSet<>();

    public Marque(String nom, LocalDate dateCreation) {
        this.nom = nom;
        this.dateCreation = dateCreation;
    }
}
