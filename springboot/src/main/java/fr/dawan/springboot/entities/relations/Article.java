package fr.dawan.springboot.entities.relations;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import fr.dawan.springboot.enums.Emballage;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity
@Table(name="articles")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Include
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable = false,length=100)
    private String description;
    
    @Column(nullable = false)
    private double prix;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private Emballage emballage;    
    
    @ManyToOne
//    @JoinColumn(name="fk_marque") -> pour renommer la colonne de jointure
    @Exclude
    private Marque marque; 
    
    @ManyToMany //(fetch=FetchType.EAGER) -> à éviter
    @Exclude
//    Pour renommer la table de jointure  
//    @JoinTable(name = "article2fournisseur",
//    joinColumns = @JoinColumn(name="fk_article"),
//    inverseJoinColumns = @JoinColumn(name="fk_fournisseur"))
    private Set<Fournisseur> fournisseurs=new HashSet<>();

    public Article(String description, double prix, Emballage emballage) {
        this.description = description;
        this.prix = prix;
        this.emballage = emballage;
    }
    
}
