package fr.dawan.springboot.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.entities.relations.Article;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.enums.Emballage;

//@Repository // -> implicite
@Transactional
public interface ArticleRepository extends JpaRepository<Article, Long> {
    // sujet find
    List<Article> findByPrix(double prix);
    
    List<Article> findByPrixLessThan(double prixMax);
    
    List<Article> findByPrixGreaterThanAndEmballage(double prixMin,Emballage emballage);
    
    List<Article> findByPrixBetween(double prixMin,double prixMax);
    
    List<Article> findByEmballageIn(Emballage... emballages);
    
    List<Article> findByDescriptionLike(String model);
    
    // IgnoreCase
    List<Article> findByDescriptionIgnoreCase(String description);
    
    List<Article> findByPrixLessThanOrderByDescriptionDescPrix(double prixMax);

    List<Article> findByMarque(Marque marque);
    
    // Expression de chemin -> uniquement avec les @OneToOne et les @ManyToOne
    List<Article> findByMarqueDateCreationAfter(LocalDate creation);
    
    List<Article> findByMarqueCharteCouleurLike(String modelCouleur);
    
    // Top ou First -> en SQL LIMIT
    Article findTopByOrderByPrixDesc(); // 1 seul objet
    
    List<Article> findTop3ByOrderByPrix();  
    
    // sujet exists
    boolean existsByEmballage(Emballage emballage);
    
    // sujet count
    int countByPrixGreaterThan(double prixMin);
   
    // sujet delete void ou int pour le type retour (nombre de ligne supprimer) 
    void deleteByMarque(Marque m);
    
    // deleteById de JPARepository ne retourne pas le nombre de ligne
    // à la place de deleteBy on peut aussi utiliser removeBy
    int removeById(long id);
    
    // JPQL
    // Si le nom du paramètre de la requête et le nom du paramètre de la méthode
    // On utilise l'annotation @Param
    @Query("SELECT a FROM Article a WHERE a.prix<:prix")
    List<Article> findPrixInfJPQL(@Param("prix") double prixMax);
    
    // paramètre nommé -> :nom paramètre
    // Si les 2 paramètres ont le même nom @Param n'est pas nécessaire 
    @Query("SELECT a FROM Article a WHERE a.prix<:prixMax")
    List<Article> findPrixInfJPQL2(double prixMax);
    
    // paramètre de position -> ?
    @Query("FROM Article a WHERE a.prix<?1 AND a.emballage=?2")
    List<Article> findPrixInfJPQL3(double prixMax,Emballage emb);
    
    // Path Expression -> @OneToOne ou @ManyToOne
    @Query("FROM Article a WHERE a.marque.nom=:nomMarque")
    List<Article> findMarqueNom(String nomMarque);
    
    // @ManyToMany ou @OneToMany -> jointure
    @Query("FROM Article a JOIN a.fournisseurs f WHERE f.nom=?1")
    List<Article> findFournisseurNom(String nomFournisseur);
    
    // SQL -> attribut nativeQuery = true
    @Query(nativeQuery = true, value="SELECT * FROM articles WHERE prix>:prixMin")
    List<Article> findPrixSupSQL(double prixMin);

    // Pagination
   Page<Article> findAllBy(Pageable page);
   
   List<Article> findByPrixGreaterThan(double prix,Pageable page);
   
   // Procedure Stockée
   // Appel Explicite
   @Procedure("GET_COUNT_BY_PRIX")
   int countInfMontant(double montant);
   
   // Appel Implicite
   @Procedure
   int get_count_by_prix(@Param("prix") double prix);
}
