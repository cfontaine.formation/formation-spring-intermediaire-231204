package fr.dawan.springboot.repositories;

import java.time.LocalDate;
import java.util.List;

import fr.dawan.springboot.entities.relations.Marque;

public interface MarqueCustomRepository {

    List<Marque> findBy(String nom, LocalDate creation);
}
