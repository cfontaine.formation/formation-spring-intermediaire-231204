package fr.dawan.springboot.services;

import fr.dawan.springboot.dtos.ArticleDto;

public interface ArticleService extends GenericService<ArticleDto, Long> {

}
