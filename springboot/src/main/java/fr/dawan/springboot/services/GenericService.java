package fr.dawan.springboot.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface GenericService<TDto,ID> {

    Page<TDto> getALL(Pageable page);
    
    TDto getById(ID id);
    
    boolean detete(ID id);
    
    TDto save(TDto dto);
    
    TDto update(TDto dto,ID id);
}
