package fr.dawan.springboot.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueService;

@Service
@Transactional
public class MarqueServiceImpl implements MarqueService {

    @Autowired
    private MarqueRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MarqueDto> getALLMarque(Pageable page) {
//        List<Marque> lst=repository.findAll(page).getContent();
//        List<MarqueDto> lstDto=new ArrayList<>();
//        for(Marque m:lst) {
//            lstDto.add(mapper.map(m, MarqueDto.class));
//        }
//        return lstDto;

        return repository.findAll(page).stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    public MarqueDto getMarqueById(long id) {
        return mapper.map(repository.findById(id).get(), MarqueDto.class);
    }

    @Override
    public List<MarqueDto> getMarqueByNom(String nom) {
        return repository.findByNomLike("%" + nom + "%").stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    public boolean deleteMarque(long id) {
        return repository.removeById(id) != 0;
    }

    @Override
    public MarqueDto save(MarqueDto marqueDto) {
        Marque marque = repository.saveAndFlush(mapper.map(marqueDto, Marque.class));
        return mapper.map(marque, MarqueDto.class);
    }

    @Override
    public MarqueDto update(MarqueDto marqueDto) {
        return repository.findById(marqueDto.getId()).map(m -> {
            m.setNom(marqueDto.getNom());
            m.setDateCreation(marqueDto.getDateCreation());
            return mapper.map(repository.saveAndFlush(m), MarqueDto.class);
        }).orElseGet(() -> {
            return mapper.map(repository.saveAndFlush(mapper.map(marqueDto, Marque.class)), MarqueDto.class);
        });
    }

}
