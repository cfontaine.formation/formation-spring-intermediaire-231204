package fr.dawan.springboot;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);
        
//      à la place de l'éxécution de la méthode de classe run
//      On peut créer une instance de SpringApplication et la personnaliser

//	    SpringApplication app=new SpringApplication(SpringbootApplication.class);
//	    app.setAddCommandLineProperties(false);
//	    app.setBannerMode(Mode.OFF);
//	    app.run(args);
    }
    
    @Bean
    ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
