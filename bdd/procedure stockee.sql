USE springboot;

-- Définir la procédure stockée
DELIMITER $
CREATE PROCEDURE GET_COUNT_BY_PRIX(IN montant DOUBLE, OUT nb_article INT)
BEGIN
	SELECT COUNT(id) INTO nb_article FROM articles WHERE prix<montant;
END $
DELIMITER ;

-- Appeler la procédure stockée GET_COUNT_BY_PRIX et placer le résultat
-- dans la variable de session @n
CALL GET_COUNT_BY_PRIX(20.0,@n);

-- afficher le contenu de la varaible @n
SELECT @n;

SHOW PROCEDURE STATUS;
