package fr.dawan.monument.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.monument.dtos.MonumentDto;
import fr.dawan.monument.services.MonumentService;

@RestController
@RequestMapping("/api/v1/monuments")
public class MonumentController {
    
    @Autowired
    private MonumentService service;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
    List<MonumentDto> getAll(Pageable page){
        return service.getAll(page);
    }
    
    @GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MonumentDto> getById(@PathVariable long id) {
        try {
            return ResponseEntity.ok().body(service.getById(id));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
    
    @DeleteMapping(value="/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteById(@PathVariable long id) {
        if(service.deleteById(id)) {
            return new ResponseEntity<String>("Le momunent id="+id+ " est suprimée", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<String>("Le momunent id="+id+ " n'existe pas", HttpStatus.NOT_FOUND);
        }
            
    }
    
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public MonumentDto create(@RequestBody MonumentDto monumentDto) {
        return service.save(monumentDto);
    }
    
    @PutMapping(value="/{id}",consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public MonumentDto update(@PathVariable long id,@RequestBody MonumentDto monumentDto ) {
        monumentDto.setId(id);
        return service.update(monumentDto);
    }
    
    @PostMapping(value="/upload/{id}", consumes=MediaType.MULTIPART_FORM_DATA_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public MonumentDto uploadImage(@PathVariable long id, @RequestParam("photo") MultipartFile file) throws IOException {
        MonumentDto mDto=service.getById(id);
        System.out.println(file.getOriginalFilename());
        mDto.setPhoto(file.getBytes());
        return service.update(mDto);
    }
    
    @PostMapping(consumes=MediaType.MULTIPART_FORM_DATA_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public MonumentDto uploadImage(@RequestParam("monument") String monumentJson, @RequestParam("photo") MultipartFile file) throws IOException {
        MonumentDto monumentDto=objectMapper.readValue(monumentJson, MonumentDto.class);
        monumentDto.setPhoto(file.getBytes());
        return service.save(monumentDto);
    }
}
