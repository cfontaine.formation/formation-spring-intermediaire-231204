package fr.dawan.monument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import fr.dawan.monument.repositories.EtiquetteRepository;
import fr.dawan.monument.repositories.MonumentRepository;

// @Component
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    private MonumentRepository monumentRepository;
    
    @Autowired
    private EtiquetteRepository etiquetteRepository;
    
    @Override
    public void run(String... args) throws Exception {
        System.out.println("-----------------------------");
        monumentRepository.findByAnneeConstructionBetweenOrderByAnneeConstructionDesc(2000,2023).forEach(System.out::println);
        System.out.println("-----------------------------");
        monumentRepository.findByNomLike("Tour%").forEach(System.out::println);
        System.out.println("-----------------------------");
        System.out.println(monumentRepository.findByCoordonneLongitudeAndCoordonneLatitude(4.850833333333333,45.7633333));
        System.out.println("-----------------------------");
        monumentRepository.findByLocalisationPaysIn("France","Italie").forEach(System.out::println);
        System.out.println("-----------------------------");
        monumentRepository.findTop5ByOrderByAnneeConstruction().forEach(System.out::println);
        System.out.println("-----------------------------");
        monumentRepository.findEtiquetteIntitule("Statue").forEach(System.out::println);
        monumentRepository.findByEtiquettesIntitule("Exposition universelle").forEach(System.out::println);
        System.out.println("-----------------------------");
        etiquetteRepository.findByIntituleLike("__a%").forEach(System.out::println);
    }

}
