package fr.dawan.monument.dtos;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

public class MonumentDto {

    @Include
    private long id;
    
    private String nom;
    
    private int anneeConstruction;
    
    @Exclude
    private byte[] photo;
    
    private String description;

//    private double coordonneLongitude;
//    
//    private double coordonneLatitude;
}
