package fr.dawan.monument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.monument.dtos.MonumentDto;
import fr.dawan.monument.services.MonumentService;

//@Component
public class ServiceRunner implements CommandLineRunner {

    @Autowired
    private MonumentService monumentService;
    
    @Override
    public void run(String... args) throws Exception {
        monumentService.getAll(Pageable.unpaged()).forEach(System.out::println);
        System.out.println(monumentService.getById(1L));
        MonumentDto dto=new MonumentDto(0,"test",2023,null,"description"/*,2.0,2.0*/);
        dto=monumentService.save(dto);
        System.out.println(dto);
        dto.setNom("nom update");
       // dto.setCoordonneLatitude(1.0);
        dto=monumentService.update(dto);
        System.out.println(dto);
        monumentService.deleteById(dto.getId());
        monumentService.getAll(Pageable.unpaged()).forEach(System.out::println);
    }

}
