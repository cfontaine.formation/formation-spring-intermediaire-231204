package fr.dawan.monument.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.monument.entities.Coordonne;

public interface CoordonneRepository extends JpaRepository<Coordonne, Long> {

}
