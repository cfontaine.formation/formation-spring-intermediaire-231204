

package fr.dawan.monument.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.dawan.monument.entities.Monument;

public interface MonumentRepository extends JpaRepository<Monument, Long> {
    
    // rechercher les monuments en fonction d'un interval sur les années de construction trie par année de construction décroissant
    List<Monument> findByAnneeConstructionBetweenOrderByAnneeConstructionDesc(int anneeMin,int anneeMax);

    // rechercher les monuments en fonction d'un modèle(LIKE) sur le nom du monument
    List<Monument> findByNomLike(String model);
    
    Monument findByCoordonneLongitudeAndCoordonneLatitude(double longitude,double latitude);
    
    List<Monument> findByLocalisationPaysIn(String ... pays );

    List<Monument> findTop5ByOrderByAnneeConstruction();
    
    @Query("FROM Monument m JOIN m.etiquettes e WHERE e.intitule=:intitule ")
    List<Monument> findEtiquetteIntitule(String intitule);
    
    List<Monument> findByEtiquettesIntitule(String intitule);

    int removeById(long id);
}
