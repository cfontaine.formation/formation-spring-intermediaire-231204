package fr.dawan.monument.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.monument.dtos.MonumentDto;
import fr.dawan.monument.entities.Monument;
import fr.dawan.monument.repositories.MonumentRepository;
import fr.dawan.monument.services.MonumentService;

@Service
@Transactional
public class MonumentServiceImpl implements MonumentService {

    @Autowired
    private MonumentRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MonumentDto> getAll(Pageable page) {
        return repository.findAll(page).stream().map(m -> mapper.map(m, MonumentDto.class)).toList();
    }

    @Override
    public MonumentDto getById(long id) {
        return mapper.map(repository.findById(id).get(), MonumentDto.class);
    }

    @Override
    public List<MonumentDto> getByNom(String nom) {
        return repository.findByNomLike(nom).stream().map(m -> mapper.map(m, MonumentDto.class)).toList();
    }

    @Override
    public List<MonumentDto> getByPays(String... pays) {
        return repository.findByLocalisationPaysIn(pays).stream().map(m -> mapper.map(m, MonumentDto.class)).toList();
    }

    @Override
    public boolean deleteById(long id) {
        return repository.removeById(id) != 0;
    }

    @Override
    public MonumentDto save(MonumentDto dto) {
        return mapper.map(repository.saveAndFlush(mapper.map(dto, Monument.class)), MonumentDto.class);
    }

    @Override
    public MonumentDto update(MonumentDto dto) {
        return repository.findById(dto.getId()).map(m -> {
            m.setNom(dto.getNom());
            m.setAnneeConstruction(dto.getAnneeConstruction());
            m.setDescription(dto.getDescription());
            m.setPhoto(dto.getPhoto());
//            m.getCoordonne().setLatitude(dto.getCoordonneLatitude());
//            m.getCoordonne().setLongitude(dto.getCoordonneLongitude());
            return mapper.map(repository.saveAndFlush(m), MonumentDto.class);
        }).orElseGet(() -> {
            return mapper.map(repository.saveAndFlush(mapper.map(dto, Monument.class)), MonumentDto.class);
        });
    }

}
